"""Module to test parser.py"""
from pathlib import Path

import pytest
from flywheel_gear_toolkit import GearToolkitContext

from fw_gear_fast_surfer.parser import parse_config


@pytest.mark.parametrize(
    "debug, use_gpu", [(False, True), (False, False), (True, True), (True, False)]
)
def test_parse_config(debug, use_gpu):
    context = GearToolkitContext(input_args=[])
    context.config_json = {
        "config": {"debug": debug, "use-gpu": use_gpu},
        "inputs": {
            "T1w": {
                "base": "file",
                "location": {
                    "path": "/path/to/file.nii.gz",
                },
            }
        },
    }
    input_file, op_use_gpu, op_debug = parse_config(context)

    assert input_file == Path("/path/to/file.nii.gz")
    assert op_use_gpu is use_gpu
    assert op_debug is debug


def test_parse_config_raise_if_not_nii_gz():
    context = GearToolkitContext(input_args=[])
    context.config_json = {
        "config": {"debug": False, "use-gpu": True},
        "inputs": {
            "T1w": {
                "base": "file",
                "location": {
                    "path": "/path/to/file.nii",
                },
            }
        },
    }
    with pytest.raises(ValueError):
        parse_config(context)
