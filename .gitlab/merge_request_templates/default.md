# Tasks

## General

- [ ] [Release Notes](./CONTRIBUTING.md#populating-release-notes)

- [ ] [Changelog](./CONTRIBUTING.md#adding-changelog-entry)

- [ ] Tests

## Gear specific (if applicable)

- [ ] Example run:

- [ ] Regression test for gear added to Qase
