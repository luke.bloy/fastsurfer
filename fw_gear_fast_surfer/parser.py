"""Parser module to parse gear config.json."""
from pathlib import Path
from typing import Tuple

from flywheel_gear_toolkit import GearToolkitContext


# This function mainly parses gear_context's config.json file and returns relevant inputs and options.
def parse_config(
    gear_context: GearToolkitContext,
) -> Tuple[str, str]:
    """Get nifti_file, use_gpu flag and debug option"""

    use_gpu = gear_context.config.get("use-gpu")
    debug = gear_context.config.get("debug")
    input_file = Path(gear_context.get_input_path("T1w"))
    if not input_file.name.endswith(".nii.gz"):
        raise ValueError(
            f"input_file must be of type nifti gz (.nii.gz), " f"({input_file} found)"
        )
    # with open(gear_context.get_input_path("text-input"), "r") as text_file:
    #    text = " ".join(text_file.readlines())
    # input_file = Path
    return input_file, use_gpu, debug
