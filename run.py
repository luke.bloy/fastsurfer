#!/usr/bin/env python
"""The run script"""
import logging
import subprocess
import sys
import time
from pathlib import Path

from flywheel_gear_toolkit import GearToolkitContext

from fw_gear_fast_surfer.parser import parse_config

log = logging.getLogger(__name__)


def main(context: GearToolkitContext) -> None:  # pragma: no cover
    """Parses config and run"""

    # Call parse_config to extract the args, kwargs from the context
    # (e.g. config.json).
    input_file, use_gpu, _ = parse_config(context)

    file_name = input_file.name

    cmd = [
        "/venv/bin/python",
        "/FastSurferCNN/eval.py",
        "--i_dir",
        "/flywheel/v0/input",
        "--o_dir",
        "/flywheel/v0/output",
        "--in_name",
        file_name,
        "--out_name",
        "aparc+aseg.nii.gz",
    ]

    if use_gpu == False:
        cmd.append("--no_cuda")

    log.info(f"Calling...\n{' '.join(cmd)}")
    # Exit the python script (and thus the container) with the exit
    # code returned by example_gear.main.run function.

    process = subprocess.Popen(
        cmd, cwd="/FastSurferCNN", stdout=subprocess.PIPE, stderr=subprocess.PIPE
    )
    # Stream the output from p.communicate so the user can monitor the progress continuously.
    # This approach is taken from:
    # https://gitlab.com/flywheel-io/flywheel-apps/nifti-to-dicom/-/blob/0.1.0/fw_gear_nifti_to_dicom/pixelmed.py
    while True:
        time.sleep(5)
        if process.poll() is None:
            output, _ = process.communicate()
            print(output.decode("utf-8").strip() + "\n")
        else:
            if process.returncode != 0:
                log.error(f"FastSurfer failed with exit_code: {process.returncode}")
                raise SystemExit(1)
            else:
                log.info("FastSurfer completed")
                sys.exit(process.returncode)


# Only execute if file is run as main, not when imported by another module
if __name__ == "__main__":  # pragma: no cover
    # Get access to gear config, inputs, and sdk client if enabled.
    with GearToolkitContext() as gear_context:

        # Initialize logging, set logging level based on `debug` configuration
        # key in gear config.
        gear_context.init_logging()

        # Pass the gear context into main function defined above.
        main(gear_context)
